package io.bugman;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AppTest {

    private final String[] inputNumbers;
    private final String expectedOutput;

    public AppTest(String[] inputNumbers, String expectedOutput) {
        this.inputNumbers = inputNumbers;
        this.expectedOutput = expectedOutput;
    }
    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"5", "2", "8", "1", "4", "3", "6", "9", "0", "7"},
                        "Sorted numbers: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]"}
        });
    }
    @Test(expected = IllegalArgumentException.class)
    public void testNoArguments() {
        App.main(new String[]{});
    }

    @Test
    public void testSingleArgument() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;

        try {
            System.setOut(new PrintStream(outputStream));
            App.main(new String[]{"5"});
            assertEquals("Sorted numbers: [5]", outputStream.toString().trim());
        } finally {
            System.setOut(originalOut);
        }
    }

    @Test
    public void testTenArguments() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;

        try {
            System.setOut(new PrintStream(outputStream));
            App.main(inputNumbers);
            assertEquals(expectedOutput, outputStream.toString().trim());
        } finally {
            System.setOut(originalOut);
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArguments() {
        App.main(new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNonIntegerArguments() {
        App.main(new String[]{"1", "2", "three"});
    }
}
