package io.bugman;

import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The Sorting App class is a Java application that takes command-line arguments as integer values,
 * sorts them in ascending order, and prints the sorted numbers to standard output. It also handles
 * various corner cases and logs relevant information and errors using Log4j.
 */
public class App
{
    // Initialize Log4j logger
    private static final Logger logger = LogManager.getLogger(App.class);

    /**
     * Entry point of the Sorting App.
     * This method takes command-line arguments as integer values, sorts them in ascending order,
     * and prints the sorted numbers to standard output. It also handles various corner cases
     * and logs relevant information and errors using Log4j.
     *
     * @param args the command-line arguments as string values
     * @throws IllegalArgumentException if no arguments are provided, more than 10 arguments are provided,
     *                                  or if a non-integer argument is provided
     */
    public static void main( String[] args ) {

        // Initialize Log4j with the configuration file
        System.setProperty("log4j.configurationFile", "log4j2.xml");

        // Corner case: No arguments provided
        if (args.length == 0) {
            logger.info("No arguments provided. Please provide up to 10 integers.");
            throw new IllegalArgumentException();
        }

        // Corner case: More than 10 arguments provided
        if (args.length > 10) {
            logger.error("Received more than 10 arguments. Please provide 10 or fewer integers.");
            throw new IllegalArgumentException();
        }

        try {
            // Parse arguments to integers
            int[] numbers = new int[args.length];
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }
            logger.info("Input is successfully parsed.");


            // Sort numbers
            Arrays.sort(numbers);
            logger.info("Numbers are successfully sorted.");


            // Print sorted numbers
            System.out.println("Sorted numbers: " + Arrays.toString(numbers));
        } catch (NumberFormatException e) {
            // Corner case: Non-integer argument provided
            logger.error("Invalid input. Please provide integers only.", e);
            throw new IllegalArgumentException();
        }
    }
}
